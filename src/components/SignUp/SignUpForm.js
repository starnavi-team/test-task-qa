import React, { Component } from 'react';
import * as ROUTES from '../../constants/routes';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import { withRouter } from 'react-router-dom';
import { withFirebase } from '../Firebase';
import { Link as RouterLink } from 'react-router-dom';

const INITIAL_STATE = {
    username: '',
    email: '',
    passwordOne: '',
    passwordTwo: '',
    error: null,
};

const SignInLink = React.forwardRef((props, ref) => (
    <RouterLink innerRef={ref} to={ROUTES.SIGN_IN} {...props} />
  ));

class SignUpForm extends Component {
    constructor(props) {
        super(props);

        this.state = { ...INITIAL_STATE };
    }

    onSubmit = event => {
        const { email, passwordOne } = this.state;

        this.props.firebase
        .doCreateUserWithEmailAndPassword(email, passwordOne)
        .then(authUser => {
            this.setState({ ...INITIAL_STATE });
            this.props.history.push(ROUTES.COMPARE);
        })
        .catch(error => {
            this.setState({ error });
        });

        event.preventDefault();
    };

    onChange = event => {
        this.setState({ [event.target.name]: event.target.value });
    };

    render() {
        const {
            username,
            email,
            passwordOne,
            passwordTwo,
            error,
          } = this.state;
      
          const isInvalid =
            passwordOne !== passwordTwo ||
            passwordOne === '' ||
            email === '' ||
            username === '';
        
        return (
            <form onSubmit={this.onSubmit} noValidate>
                <TextField
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    id="username"
                    label="Full Name"
                    name="username"
                    value={username}
                    autoComplete="username"
                    onChange={this.onChange}
                    autoFocus
                />
                <TextField
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    id="email"
                    label="Email Address"
                    name="email"
                    value={email}
                    autoComplete="email"
                    onChange={this.onChange}
                />
                <TextField
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    name="passwordOne"
                    label="Password"
                    type="password"
                    id="passwordOne"
                    value={passwordOne}
                    onChange={this.onChange}
                    autoComplete="current-password"
                />
                <TextField
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    name="passwordTwo"
                    label="Confirm Password"
                    type="password"
                    id="passwordTwo"
                    value={passwordTwo}
                    onChange={this.onChange}
                    autoComplete="current-password"
                />
                <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    disabled={isInvalid}
                    style={{
                        margin: '15px 0'
                    }}
                >
                    Sign Up
                </Button>
                {error && <p>{error.message}</p>}
                <Grid container justify="flex-end">
                    <Grid item>
                        <Link component={SignInLink} variant="body2">
                            {"Already have an account? Sign in"}
                        </Link>
                    </Grid>
                </Grid>
            </form>
        );
    }
}

export default withRouter(withFirebase(SignUpForm));